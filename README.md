# CSS Landing Page

## Description
A simple landing page with css and html showing responsive design using flexbox.


## Instructions
Just load the index.html file into your browser. Try looking at it using different viewport sizes it has a mobile and desktop view. It is only the landing page so the buttons and links do not go anywhere.


## Notes
The theme is based on the Landing Page bootstrap theme found [here](https://startbootstrap.com/theme/landing-page). While the theme files do have css in them,
this page was created without bootstrap. You can try comparing my page to the theme files and see if you find any differences. If you do, be sure to let me know.

The icons were made using GIMP so they may look a little off. Ideally I would source them online or have a Web Designer create them. 